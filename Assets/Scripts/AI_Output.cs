﻿using UnityEngine;


class AI_Output : MonoBehaviour
{
    private int voiceOK;
    private VoiceManager vm;

    [SerializeField] private bool _voiceOn = true;

    private string lastWord = string.Empty;

    void Start()
    {
        if(_voiceOn)
        {
            vm = GetComponent<VoiceManager>();
            voiceOK = vm.Init();
        }
    }

    public void Speak(string text)
    {
        if (voiceOK != 1 && lastWord != text && _voiceOn)
        {
            vm.Say(text);
            lastWord = text;
        }
    }
}