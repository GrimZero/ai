﻿using UnityEngine;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Net;

public class MailBase
{
    //private (local)
#pragma warning disable CS0618 // Type or member is obsolete
    private SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
#pragma warning restore CS0618 // Type or member is obsolete
    private string _emailAddress = "";

    public void InitialiseSMTP(string emailAddress, string password)
    {
        smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
        smtpClient.Port = 587;
        smtpClient.EnableSsl = true;
        smtpClient.Credentials = new NetworkCredential(emailAddress, password) as ICredentialsByHost;
        smtpClient.Timeout = 20000;
        smtpClient.UseDefaultCredentials = false;

        _emailAddress = emailAddress;
    }

    public void Send(MailMessage mailMessage)
    {
        ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        };

        try
        {
            smtpClient.Send(mailMessage);
            mailMessage.Dispose();
        }
        catch (SmtpException exception)
        {
            Debug.LogWarning("Exception: " + exception.ToString());
        }
    }
}
