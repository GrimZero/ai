﻿using System.Net.Mail;

public class Mail : MailBase
{
    public MailMessage CreateMail()
    {
        MailMessage mail = new MailMessage();
        return mail;
    }

    public void SetSender(MailMessage toSend, string sender)
    {
        toSend.From = new MailAddress(sender);
    }
}
