﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Net.Mail;

public class MailForm : MonoBehaviour
{
    [Space] [SerializeField] private MailComponent AI = null;

    [SerializeField] private InputField _to = null;
    [SerializeField] private InputField _cc = null;
    [SerializeField] private InputField _bcc = null;

    [SerializeField] private InputField _subjectField = null;
    [SerializeField] private TMPro.TMP_InputField _bodyField = null;

    [SerializeField] private Button _buttonSend;
    [Space] [SerializeField] private CanvasDisplayController _canvasController;

    private MailMessage mail = null;

    private void Start()
    {
        mail = AI.CreateMail();
    }

    void ClearFields()
    {
        _to.text = _cc.text = _bcc.text = _subjectField.text = _bodyField.text = string.Empty;
    }

    public void AddTo() { mail.To.Add(_to.text); _to.text = string.Empty; }
    public void AddCC() { mail.CC.Add(_cc.text); _cc.text = string.Empty; }
    public void AddBCC() { mail.Bcc.Add(_bcc.text); _bcc.text = string.Empty; }
    public void BackButton()
    {
        ClearFields();
        _canvasController.EnableElement("BaseAI", true);
    }

    public void SendOnClick()
    {
        mail.Subject = _subjectField.text;
        mail.Body = _bodyField.text;

        ClearFields();
        AI.SendMail(mail);
        _canvasController.EnableElement("BaseAI", true);
    }
}
