﻿using UnityEngine;
using UnityEditor;
using System.Net.Mail;

public class MailComponent : MonoBehaviour
{
    Mail _mail = new Mail();

    //client side setup
    [SerializeField] private string _email = "kamuikanna.kobayashi@gmail.com";
    [HideInInspector] public string Password;

    //
    private enum MailSuggestionEnum { Appointment, Request, Other }
    MailSuggestionEnum suggestion = MailSuggestionEnum.Other;


    public MailMessage CreateMail()
    {
        _mail.InitialiseSMTP(_email, Password);
        MailMessage toSend = _mail.CreateMail();

        _mail.SetSender(toSend, _email);

        return toSend;
    }

    public void SendMail(MailMessage msg)
    {
        if (Verify(msg))
        {
            _mail.Send(msg);
        }
    }

    public bool Verify(MailMessage mail)
    {
        //if there is a sender and receiver
        if (mail.To.Count > 0 || mail.From.Address != null)
        {
            if (mail.Subject == null)
            {
                Debug.LogError("Mail has no subject.");
                return false;
            }

            if (mail.Body == null)
            {
                Debug.LogError("Mail has no body.");
                return false;
            }
        }
        return true;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(MailComponent))]
public class MailComponentEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        MailComponent m = target as MailComponent;

        m.Password = EditorGUILayout.PasswordField("Password: ", m.Password);
    }
}
#endif