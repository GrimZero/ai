﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningHours : MonoBehaviour
{
    public string url;

    IEnumerator Start()
    {
        using (WWW www = new WWW(url))
        {
            yield return www;
        }
    }
}
