﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using ApiAiSDK;
using ApiAiSDK.Model;
using ApiAiSDK.Unity;
using Newtonsoft.Json;
using System.Net;
using UnityEngine.Windows.Speech;

public class AI_Input : MonoBehaviour
{
    private ApiAiUnity apiAiUnity = new ApiAiUnity();

    public InputField _inputField = null;

    private readonly JsonSerializerSettings jsonSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, };

    public AIResponse _currentOutput = new AIResponse();

    [HideInInspector] public string InputValue = string.Empty;

    private DictationRecognizer dictationRecognizer;

    IEnumerator Start()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.Microphone);
        if (!Application.HasUserAuthorization(UserAuthorization.Microphone))
        {
            throw new NotSupportedException("Microphone was not authorized by the user");
        }

        ServicePointManager.ServerCertificateValidationCallback = (a, b, c, d) => { return true; };

        const string ACCESS_TOKEN = "42a5455e313e4897a86b930509873338";
        var config = new AIConfiguration(ACCESS_TOKEN, SupportedLanguage.English);

        apiAiUnity.Initialize(config);

        //voice input
        dictationRecognizer = new DictationRecognizer();
        dictationRecognizer.InitialSilenceTimeoutSeconds = -1;

        dictationRecognizer.DictationResult += (text, confidence) =>
        {
            Debug.Log(text);
            _inputField.text = text;
            InputValue = text;
            SendText();
        };
        dictationRecognizer.Start();
    }

    private void OnDestroy()
    {
        dictationRecognizer.Dispose();
        dictationRecognizer.Stop();
    }

    void SendText()
    {
        if (InputValue !=string.Empty)
        {
            AIResponse response = apiAiUnity.TextRequest(InputValue);

            if (response != null)
            {
                _currentOutput = response;
                GetComponent<AI_Processing>()._hasAnswered = true;
            }
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Return))
        {
            InputValue = _inputField.text;
            SendText();
            _inputField.text = string.Empty;
        }
    }

    public AIResponse GetResponse()
    {
        return _currentOutput;
    }

    public void ResetInput()
    {
        InputValue = string.Empty;
    }
}
