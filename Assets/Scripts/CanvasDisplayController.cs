﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable] public class StringObjectDictionary : SerializableDictionary<string, GameObject> { }

public class CanvasDisplayController : MonoBehaviour
{
    public StringObjectDictionary CanvasSwitchableElements;

    private void Disable(GameObject gO) { gO.SetActive(false); }

    /// <summary>
    /// Enables an element from the dictionary, if the bool is set to true will also disable other elements
    /// </summary>
    /// <param name="toEnable"></param>
    /// <param name="disableOthers"></param>
    public void EnableElement(string toEnable, bool disableOthers)
    {
        if (CanvasSwitchableElements.ContainsKey(toEnable))
        {
            CanvasSwitchableElements[toEnable].SetActive(true);
        }

        if (disableOthers)
        {
            foreach (var key in CanvasSwitchableElements.Keys)
            {
                if(key != toEnable) CanvasSwitchableElements[key].SetActive(false);
            }
        }
    }
}
